<?php
$export_mail_type = &#039;tz_aweber&#039;;

/* Google Recaptcha Secret Key */
$g_secret_key = &#039;&#039;; 

/* AWeber List Name. 
Get AWeber List Name from https://www.aweber.com/users/autoresponder/manage
*/

define(&#039;aweber_list_name&#039;,&#039;&#039;);

/* ActiveCampaign API URL, API KEY and List ID. 
Get API URL, API KEY and list id from go to http://www.activecampaign.com/ > My Settings > Developers
*/

define(&#039;ac_api_url&#039;,&#039;&#039;);
define(&#039;ac_api_key&#039;,&#039;&#039;);
define(&#039;ac_api_listid&#039;,&#039;&#039;);

/* Custom Email Recipient email address &amp; Email subject line */
$tz_email = &#039;&#039;;
$tz_from_email = &#039;&#039;; 
$tz_subject = &#039;&#039;;


/* Campaign Monitor API key and List ID. 
Get CM API KEY from https://your-username.createsend.com/admin/account/
Get CM List ID from https://www.campaignmonitor.com/api/getting-started/#listid
*/

define(&#039;cm_api_key&#039;, &#039;&#039;);
define(&#039;cm_list_id&#039;, &#039;&#039;);

/* GetResponse API key and Campaign Token. 
Get GetResponse API KEY from https://app.getresponse.com/my_api_key.html
Get GetResponse Campaign Token from https://app.getresponse.com/campaign_list.html
*/

define(&#039;getresponse_api_key&#039;,&#039;&#039;); 
define(&#039;getresponse_campaign_token&#039;,&#039;&#039;); 

/* Mailchimp API key and List ID. 
Get Mailchimp API key from http://admin.mailchimp.com/account/api
Get Mailchimp List ID from http://admin.mailchimp.com/lists/
*/

define(&#039;mailchimp_api_key&#039;, &#039;&#039;); 
define(&#039;mailchimp_api_listid&#039;, &#039;&#039;);

/* MailerLite API KEY &amp; GROUP ID
Get API KEY and GROUP ID from go to https://app.mailerlite.com/integrations/api/
*/

define(&#039;ml_api_key&#039;,&#039;&#039;);
define(&#039;ml_groupid&#039;,&#039;&#039;);
?>